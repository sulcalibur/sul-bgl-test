import Vue from 'vue'
import Vuex from 'vuex'
import { getCurrentWeatherForLocations, getForoecastByLocation } from './api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appStatus: {
      loading: false,
      error: false,
      message: null,
    },
    locations: [],
    forecastByLocation: {},
    forecastByLocationStatus: {},
  },
  mutations: {
    fetchLocations (state) {
      state.appStatus = {
        loading: true,
        error: false,
        message: null,
      }
    },

    fetchLocationsSuccessful(state, locations) {
      state.locations = locations
      
      state.appStatus = {
        loading: false,
        error: false,
        message: null,
      }
    },

    fetchLocationsFailed(state, message) {
      state.appStatus = {
        loading: false,
        error: true,
        message,
      }
    },

    fetchForecastByLocation (state, id) {
      Vue.set(state.forecastByLocationStatus, id, {
        loading: true,
        error: false,
        message: null,
      })
    },

    fetchForecastByLocationSuccessful(state, payload) {
      Vue.set(state.forecastByLocation, payload.id, payload.forecast);

      Vue.set(state.forecastByLocationStatus, payload.id, {
        loading: false,
        error: false,
        message: null,
      })
    },

    fetchForecastByLocationFailed(state, payload) {
      Vue.set(state.forecastByLocationStatus, payload.id, {
        loading: false,
        error: true,
        message,
      })
    },
  },
  actions: {
    async fetchLocations ({ commit, state }) {
      commit('fetchLocations')

      try {
        const locations = await getCurrentWeatherForLocations()
        commit('fetchLocationsSuccessful', locations)
      } catch (e) {
        commit('fetchLocationsFailed', e.message);
      }
    },

    async fetchForecastByLocation ({ commit, state }, id) {
      commit('fetchForecastByLocation', id)

      try {
        const forecast = await getForoecastByLocation(id)
        commit('fetchForecastByLocationSuccessful', { id, forecast })
      } catch (e) {
        commit('fetchForecastByLocationFailed', { id, message: e.message });
      }
    },
  }
})

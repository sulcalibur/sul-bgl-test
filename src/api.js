const cities = [2643743, 4192205, 5107152, 3337389, 2988507];

const baseUrl = 'http://api.openweathermap.org/data/2.5';
const appId = '1aad50c943fab2fad1a915a07a4d3aca';

export const getCurrentWeatherForLocations = async () => {
  const url = `${baseUrl}/group?id=${cities.join(',')}&appid=${appId}`;
  const response = await fetch(url);

  if (response.status !== 200) {
    throw new Error('There was an error retrieving weather data at this time.');
  }

  const data = await response.json();
  return data.list || [];
};

export const getForoecastByLocation = async id => {
  const url = `${baseUrl}/forecast?id=${id}&appid=${appId}`;
  const response = await fetch(url);

  if (response.status !== 200) {
    throw new Error('There was an error retrieving data for this location.');
  }

  const data = await response.json();
  return data.list || [];
}